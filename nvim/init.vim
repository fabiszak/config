" Fabiszak Adrian
" https://gitlab.com/fabiszak

" => Plugins
call plug#begin()
	"{{ File management }}
	Plug 'preservim/nerdtree'	" Nerd Tree
	Plug 'ryanoasis/vim-devicons' " vim-devicons
call plug#end()

" => General Settings
syntax on
set nu
set rnu

" => Remap Keys
" Remap ESC to ii lock
:imap ii <Esc>

" => NERDTree
" Uncomment to autostart the NERDTree
" autocmd vimenter * NERDTree
map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeDirArrowExpandable = '►'
let g:NERDTreeDirArrowCollapsible = '▼'
let NERDTreeShowLineNumbers=1
let NERDTreeShowHidden=1
let NERDTreeMinimalUI = 1
let g:NERDTreeWinSize=38

" => auto xrdb Xresources
" Run xrdb whenever Xdefaults or Xresources are updated.
autocmd BufWritePost *Xresources,*Xdefaults !xrdb %

